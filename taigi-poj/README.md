# Emacs 臺語白話字輸入法
本輸入法使用 Quail 簡單實現在 Emacs 上輸入帶調號的臺語白話字，不支援漢字和詞彙輸入。<br />
在每個字的最後輸入 1 ~ 8 選擇聲調，按鍵`d`輸入連字符`-`，按`d` + `Space`則輸出字母`d`。

Pún su-ji̍p-hoat sú-iōng Quail kán-tán si̍t-hiān tī Emacs phah Tâi-gí Pe̍h-ōe-jī. Bô chi-oān Hàn-jī kap sû-tián.<br />
Tī múi chi̍t jī āu phah sò͘-jī lâi kā siaⁿ-tiāu thiap--khì-lih. Phah `d` su-ji̍p `-`, phah `d` + `Space` chiah-ē chhut `d`.

`Sudjip8dhoat` --> `Su-ji̍p-hoat` （輸入法）

`Theh8 mih8dkiann7` --> `The̍h mi̍h-kiāⁿ` （提物件）

`Oo7da2 Han1dchi5` --> `Ō͘-á Han-chî` （芋仔蕃薯）

`Chiah8dpa2ddboe7?`--> `Chia̍h-pá--bōe?` （食飽袂？）

部分字型可能無法正常顯示所有白話字，推薦使用 *DejaVu Sans Mono*。可鍵入`M-x menu-set-font`選擇並瀏覽字型。 

白話字中的結合字元：<br />
`COMBINING DOT ABOVE RIGHT (U+0358)` 為右上有小圓點的`O`<br />
`COMBINING VERTICAL LINE ABOVE (U+030D)`用於標示第八調<br />
`COMBINING MACRON (U+0304)`用於標示`m`和`ng`的第七調，其餘母音的第七調並無使用此字元<br />
`SUPERSCRIPT LATIN SMALL LETTER N (U+207F)`用於標示鼻音

## 安裝和使用
將本`taigi-poj.el`檔案下載後放置到`~/.emacs.d/private/`之中（或自訂路徑）

在設定檔（通常為`init.el`）添加下列代碼：

`(package-initialize)`<br />
`(add-to-list 'load-path "~/.emacs.d/private/")`<br />
`(load "...其他輸入法檔案...)`<br />
`(load "taigi-poj.el")`

輸入`M-x set-input-method`後選擇 `taigi-poj`<br />
透過`C-\`關閉或開啟輸入法

### Spacemacs
請至`~/.spacemacs`（指令`SPC f e d`）將上方的代碼添加在`user-config`欄位：

```
(defun dotspacemacs/user-config ()
  "...說明文字..."
  (package-initialize)
  (add-to-list 'load-path "~/.emacs.d/private/")
  (load "...其他輸入法檔案...)
  (load "taigi-poj.el")
  )
```

# Emacs Taiwanese Hokkien input method
This is an Emacs input method for [Tâi-gí Pe̍h-ōe-jī](https://en.wikipedia.org/wiki/Pe%CC%8Dh-%C5%8De-j%C4%AB) (an Taiwanese Hokkien writing system).<br />
Put number 1 ~ 8 at the end of the words to attach the tones. Typing `d` inputs `-`, and typing `d` + `Space` inputs `d`.<br />
D is one of the unused letters in Taiwanese Hokkien script.

Examples:

`Sudjip8dhoat` --> `Su-ji̍p-hoat` (Input method)

`Theh8 mih8dkiann7` --> `The̍h mi̍h-kiāⁿ` (To take something)

`Oo7da2 Han1dchi5` --> `Ō͘-á Han-chî` (Taro and sweet potato)

`Chiah8dpa2ddboe7?`--> `Chia̍h-pá--bōe?` (Have you eaten yet?)
