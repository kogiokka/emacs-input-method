;; taigi-poj.el -- an input method for Taiwanese Hokkien script "Pe̍h-ōe-jī".
;;
;; GNU General Public License v3.0
;; Author: 2018 by kogiokka
;;
;; More info in README.md


(require 'quail)
(quail-define-package "taigi-poj" "UTF-8" "Tâi-gí POJ" t nil nil t)
(quail-define-rules
 ("d" "-")
 ("d " "d")

;;;;;; Starting with "a" ;;;;;;
 ("a" ["a" "á" "à" "a" "â" "ǎ" "ā" "a̍"])
 ("ann" ["aⁿ" "áⁿ" "àⁿ" "aⁿ" "âⁿ" "ǎⁿ" "āⁿ" "a̍ⁿ"])
 ("annh" ["aⁿh" "áⁿh" "àⁿh" "aⁿh" "âⁿh" "ǎⁿh" "āⁿh" "a̍ⁿh"])
 ("am" ["am" "ám" "àm" "am" "âm" "ǎm" "ām" "a̍m"])
 ("an" ["an" "án" "àn" "an" "ân" "ǎn" "ān" "a̍n"])
 ("ang" ["ang" "áng" "àng" "ang" "âng" "ǎng" "āng" "a̍ng"])
 ;; ai
 ("ai" ["ai" "ái" "ài" "ai" "âi" "ǎi" "āi" "a̍i"])
 ("aih" ["aih" "áih" "àih" "aih" "âih" "ǎih" "āih" "a̍ih"])
 ("ainn" ["aiⁿ" "áiⁿ" "àiⁿ" "aiⁿ" "âiⁿ" "ǎiⁿ" "āiⁿ" "a̍iⁿ"])
 ("ainnh" ["aiⁿh" "áiⁿh" "àiⁿh" "aiⁿh" "âiⁿh" "ǎiⁿh" "āiⁿh" "a̍iⁿh"])
 ;; au
 ("au" ["au" "áu" "àu" "au" "âu" "ǎu" "āu" "a̍u"])
 ("auh" ["auh" "áuh" "àuh" "auh" "âuh" "ǎuh" "āuh" "a̍uh"])
 ("auⁿ" ["auⁿ" "áuⁿ" "àuⁿ" "auⁿ" "âuⁿ" "ǎuⁿ" "āuⁿ" "a̍uⁿ"])
 ;; hktp
 ("ah8" ["a̍h"])
 ("ak8" ["a̍k"])
 ("at8" ["a̍t"])
 ("ap8" ["a̍p"])
 ;; CAP "A"
 ("A" ["A" "Á" "À" "A" "Â" "Ǎ" "Ā" "A̍"])
 ("Ann" ["Aⁿ" "Áⁿ" "Àⁿ" "Aⁿ" "Âⁿ" "Ǎⁿ" "Āⁿ" "A̍ⁿ"])
 ("Annh" ["Aⁿh" "Áⁿh" "Àⁿh" "Aⁿh" "Âⁿh" "Ǎⁿh" "Āⁿh" "A̍ⁿh"])
 ("An" ["An" "Án" "Àn" "An" "Ân" "Ǎn" "Ān" "A̍n"])
 ("Ang" ["Ang" "Áng" "Àng" "Ang" "Âng" "Ǎng" "Āng" "A̍ng"])
 ;; ai
 ("Ai" ["Ai" "Ái" "Ài" "Ai" "Âi" "Ǎi" "Āi" "A̍i"])
 ("Aih" ["Aih" "Áih" "Àih" "Aih" "Âih" "Ǎih" "Āih" "A̍ih"])
 ("Ainn" ["Aiⁿ" "Áiⁿ" "Àiⁿ" "Aiⁿ" "Âiⁿ" "Ǎiⁿ" "Āiⁿ" "A̍iⁿ"])
 ("Ainnh" ["Aiⁿh" "Áiⁿh" "Àiⁿh" "Aiⁿh" "Âiⁿh" "Ǎiⁿh" "Āiⁿh" "A̍iⁿh"])
 ;; au
 ("Au" ["Au" "Áu" "Àu" "Au" "Âu" "Ǎu" "Āu" "A̍u"])
 ("Auh" ["Auh" "Áuh" "Àuh" "Auh" "Âuh" "Ǎuh" "Āuh" "A̍uh"])
 ("Auⁿ" ["Auⁿ" "Áuⁿ" "Àuⁿ" "Auⁿ" "Âuⁿ" "Ǎuⁿ" "Āuⁿ" "A̍uⁿ"])
 ;; hktp
 ("Ah8" ["A̍h"])
 ("Ak8" ["A̍k"])
 ("At8" ["A̍t"])
 ("Ap8" ["A̍p"])

 ;;;;;; Starting with "e" ;;;;;;
 ("e" ["e" "é" "è" "e" "ê" "ě" "ē" "e̍"])
 ("enn" ["eⁿ" "éⁿ" "èⁿ" "eⁿ" "êⁿ" "ěⁿ" "ēⁿ" "e̍ⁿ"])
 ("ennh" ["eⁿh" "éⁿh" "èⁿh" "eⁿh" "êⁿh" "ěⁿh" "ēⁿh" "e̍ⁿh"])
 ("eng" ["eng" "éng" "èng" "eng" "êng" "ěng" "ēng" "e̍ng"])
 ;; hktp
 ("eh8" ["e̍h"])
 ("ek8" ["e̍k"])
 ;; CAP "E"
 ("E" ["E" "É" "È" "E" "Ê" "Ě" "Ē" "E̍"])
 ("Enn" ["Eⁿ" "Éⁿ" "Èⁿ" "Eⁿ" "Êⁿ" "Ěⁿ" "Ēⁿ" "E̍ⁿ"])
 ("Ennh" ["Eⁿh" "Éⁿh" "Èⁿh" "Eⁿh" "Êⁿh" "Ěⁿh" "Ēⁿh" "E̍ⁿh"])
 ("Eng" ["Eng" "Éng" "Èng" "Eng" "Êng" "Ěng" "Ēng" "E̍ng"])
 ;; hktp
 ("Eh8" ["E̍h"])
 ("Ek8" ["E̍k"])

 ;;;;;; Starting with "i" ;;;;;;
 ("i" ["i" "í" "ì" "i" "î" "ǐ" "ī" "i̍"])
 ("im" ["im" "ím" "ìm" "im" "îm" "ǐm" "īm" "i̍m"])
 ("in" ["in" "ín" "ìn" "in" "în" "ǐn" "īn" "i̍n"])
 ("inn" ["iⁿ" "íⁿ" "ìⁿ" "iⁿ" "îⁿ" "ǐⁿ" "īⁿ" "i̍ⁿ"])
 ("iu" ["iu" "iú" "iù" "iu" "iû" "iǔ" "iū" "iu̍"])
 ("iunn" ["iuⁿ" "iúⁿ" "iùⁿ" "iuⁿ" "iûⁿ" "iǔⁿ" "iūⁿ" "iu̍ⁿ"])

 ("ia" ["ia" "iá" "ià" "ia" "iâ" "iǎ" "iā" "ia̍"])
 ("iau" ["iau" "iáu" "iàu" "iau" "iâu" "iǎu" "iāu" "ia̍u"])
 ("iam" ["iam" "iám" "iàm" "iam" "iâm" "iǎm" "iām" "ia̍m"])
 ("ian" ["ian" "ián" "iàn" "ian" "iân" "iǎn" "iān" "ia̍n"])
 ("iann" ["iaⁿ" "iáⁿ" "iàⁿ" "iaⁿ" "iâⁿ" "iǎⁿ" "iāⁿ" "ia̍ⁿ"])
 ("iang" ["iang" "iáng" "iàng" "iang" "iâng" "iǎng" "iāng" "ia̍ng"])

 ("io" ["io" "ió" "iò" "io" "iô" "iǒ" "iō" "io̍"])
 ("ion" ["ion" "ión" "iòn" "ion" "iôn" "iǒn" "iōn" "io̍n"])
 ("ioh" ["ioh" "ióh" "iòh" "ioh" "iôh" "iǒh" "iōh" "io̍h"])
 ("iok" ["iok" "iók" "iòk" "iok" "iôk" "iǒk" "iōk" "io̍k"])
 ("iong" ["iong" "ióng" "iòng" "iong" "iông" "iǒng" "iōng" "io̍ng"])

 ;; hktp
 ("ih8" ["i̍h"])
 ("it8" ["i̍t"])
 ("ip8" ["i̍p"])
 ("iah8" ["ia̍h"])
 ("iak8" ["ia̍k"])
 ("iat8" ["ia̍t"])
 ("iap8" ["ia̍p"])
 ("ioh8" ["io̍h"])
 ("iok8" ["io̍k"])
 ("iauh8" ["iau̍h"])
 ("iunnh8" ["iu̍ⁿh"])

 ;; CAP "I"
 ("I" ["I" "Í" "Ì" "I" "Î" "Ǐ" "Ī" "I̍"])
 ("Im" ["Im" "Ím" "Ìm" "Im" "Îm" "Ǐm" "Īm" "I̍m"])
 ("In" ["In" "Ín" "Ìn" "In" "În" "Ǐn" "Īn" "I̍n"])
 ("Inn" ["Iⁿ" "Íⁿ" "Ìⁿ" "Iⁿ" "Îⁿ" "Ǐⁿ" "Īⁿ" "I̍ⁿ"])
 ("Iu" ["Iu" "Iú" "Iù" "Iu" "Iû" "Iǔ" "Iū" "Iu̍"])
 ("Iunn" ["Iuⁿ" "Iúⁿ" "Iùⁿ" "Iuⁿ" "Iûⁿ" "Iǔⁿ" "Iūⁿ" "Iu̍ⁿ"])

 ("Ia" ["Ia" "Iá" "Ià" "Ia" "Iâ" "Iǎ" "Iā" "Ia̍"])
 ("Iau" ["Iau" "Iáu" "Iàu" "Iau" "Iâu" "Iǎu" "Iāu" "Ia̍u"])
 ("Iam" ["Iam" "Iám" "Iàm" "Iam" "Iâm" "Iǎm" "Iām" "Ia̍m"])
 ("Ian" ["Ian" "Ián" "Iàn" "Ian" "Iân" "Iǎn" "Iān" "Ia̍n"])
 ("Iann" ["Iaⁿ" "Iáⁿ" "Iàⁿ" "Iaⁿ" "Iâⁿ" "Iǎⁿ" "Iāⁿ" "Ia̍ⁿ"])
 ("Iang" ["Iang" "Iáng" "Iàng" "Iang" "Iâng" "Iǎng" "Iāng" "Ia̍ng"])

 ("Io" ["Io" "Ió" "Iò" "Io" "Iô" "Iǒ" "Iō" "Io̍"])
 ("Ion" ["Ion" "Ión" "Iòn" "Ion" "Iôn" "Iǒn" "Iōn" "Io̍n"])
 ("Iong" ["Iong" "Ióng" "Iòng" "Iong" "Iông" "Iǒng" "Iōng" "Io̍ng"])

 ;; hktp
 ("Ih8" ["I̍h"])
 ("It8" ["I̍t"])
 ("Ip8" ["I̍p"])
 ("Iah8" ["Ia̍h"])
 ("Iak8" ["Ia̍k"])
 ("Iat8" ["Ia̍t"])
 ("Iap8" ["Ia̍p"])
 ("Ioh8" ["Io̍h"])
 ("Iok8" ["Io̍k"])
 ("Iauh8" ["Iau̍h"])
 ("Iunnh8" ["Iu̍ⁿh"])

;;;;;; Starting with "u" ;;;;;;
 ("u" ["u" "ú" "ù" "u" "û" "ǔ" "ū" "u̍"])
 ("un" ["un" "ún" "ùn" "un" "ûn" "ǔn" "ūn" "u̍n"])
 ("unn" ["uⁿ" "úⁿ" "ùⁿ" "uⁿ" "ûⁿ" "ǔⁿ" "ūⁿ" "u̍ⁿ"])
 ("ui" ["ui" "úi" "ùi" "ui" "ûi" "ǔi" "ūi" "u̍i"])
 ;; hktp
 ("ut8" ["u̍t"])
 ("uh8" ["u̍h"])
 ;; CAP "U"
 ("U" ["U" "Ú" "Ù" "U" "Û" "Ǔ" "Ū" "U̍"])
 ("Un" ["Un" "Ún" "Ùn" "Un" "Ûn" "Ǔn" "Ūn" "U̍n"])
 ("Unn" ["Uⁿ" "Úⁿ" "Ùⁿ" "Uⁿ" "Ûⁿ" "Ǔⁿ" "Ūⁿ" "U̍ⁿ"])
 ("Ui" ["Ui" "Úi" "Ùi" "Ui" "Ûi" "Ǔi" "Ūi" "U̍i"])
 ;; hktp
 ("Ut8" ["U̍t"])
 ("Uh8" ["U̍h"])

;;;;;; Starting with "o" ;;;;;;
 ("o" ["o" "ó" "ò" "o" "ô" "ǒ" "ō" "o̍"])
 ("oo" ["o͘" "ó͘" "ò͘" "o͘" "ô͘" "ǒ͘" "ō͘" "o̍͘"])
 ("Oo " ["O͘ " "Ó͘ " "Ò͘ " "O͘ " "Ô͘ " "Ǒ͘ " "Ō͘ " "O̍͘ "])
 ("onn" ["oⁿ" "óⁿ" "òⁿ" "oⁿ" "ôⁿ" "ǒⁿ" "ōⁿ" "o̍ⁿ"])
 ("oa" ["oa" "óa" "òa" "oa" "ôa" "ǒa" "ōa" "o̍a"])
 ("oe" ["oe" "óe" "òe" "oe" "ôe" "ǒe" "ōe" "o̍e"])
 ("oan" ["oan" "oán" "oàn" "oan" "oân" "oǎn" "oān" "oa̍n"])
 ("oann" ["oaⁿ" "oáⁿ" "oàⁿ" "oaⁿ" "oâⁿ" "oǎⁿ" "oāⁿ" "oa̍ⁿ"])
 ("oang" ["oang" "oáng" "oàng" "oang" "oâng" "oǎng" "oāng" "oa̍ng"])
 ("ong" ["ong" "óng" "òng" "ong" "ông" "ǒng" "ōng" "o̍ng"])
 ("oai" ["oai" "oái" "oài" "oai" "oâi" "oǎi" "oāi" "oa̍i"])
 ("oainn" ["oaiⁿ" "oáiⁿ" "oàiⁿ" "oaiⁿ" "oâiⁿ" "oǎiⁿ" "oāiⁿ" "oa̍iⁿ"])

 ;; hktp
 ("ooh" ["o͘h" "ó͘h" "ò͘h" "o͘h" "ô͘h" "ǒ͘h" "ō͘h" "o̍͘h"])
 ("onnh" ["oⁿh" "óⁿh" "òⁿh" "oⁿh" "ôⁿh" "ǒⁿh" "ōⁿh" "o̍ⁿh"])
 ("oaih" ["oaih" "oáih" "oàih" "oaih" "oâih" "oǎih" "oāih" "oa̍ih"])

 ("oh8" ["oh"])
 ("ok8" ["o̍k"])
 ("oah8" ["oa̍h"])
 ("oeh8" ["oe̍h"])
 ("oat8" ["oa̍t"])

 ;; CAP "O"
 ("O" ["O" "Ó" "Ò" "O" "Ô" "Ǒ" "Ō" "O̍"])
 ("Oo" ["O͘" "Ó͘" "Ò͘" "O͘" "Ô͘" "Ǒ͘" "Ō͘" "O̍͘"])
 ("Oo " ["O͘ " "Ó͘ " "Ò͘ " "O͘ " "Ô͘ " "Ǒ͘ " "Ō͘ " "O̍͘ "])
 ("Onn" ["Oⁿ" "Óⁿ" "Òⁿ" "Oⁿ" "Ôⁿ" "Ǒⁿ" "Ōⁿ" "O̍ⁿ"])
 ("Oa" ["Oa" "Óa" "Òa" "Oa" "Ôa" "Ǒa" "Ōa" "O̍a"])
 ("Oe" ["Oe" "Óe" "Òe" "Oe" "Ôe" "Ǒe" "Ōe" "O̍e"])
 ("Ong" ["Ong" "Óng" "Òng" "Ong" "Ông" "Ǒng" "Ōng" "O̍ng"])
 ("Oan" ["Oan" "Oán" "Oàn" "Oan" "Oân" "Oǎn" "Oān" "Oa̍n"])
 ("Oann" ["Oaⁿ" "Oáⁿ" "Oàⁿ" "Oaⁿ" "Oâⁿ" "Oǎⁿ" "Oāⁿ" "Oa̍ⁿ"])
 ("Oang" ["Oang" "Oáng" "Oàng" "Oang" "Oâng" "Oǎng" "Oāng" "Oa̍ng"])
 ("Oai" ["Oai" "Oái" "Oài" "Oai" "Oâi" "Oǎi" "Oāi" "Oa̍i"])
 ("Oainn" ["Oaiⁿ" "Oáiⁿ" "Oàiⁿ" "Oaiⁿ" "Oâiⁿ" "Oǎiⁿ" "Oāiⁿ" "Oa̍iⁿ"])

 ;; hktp
 ("Ooh" ["O͘h" "Ó͘h" "Ò͘h" "O͘h" "Ô͘h" "Ǒ͘h" "Ō͘h" "O̍͘h"])
 ("Onnh" ["Oⁿh" "Óⁿh" "Òⁿh" "Oⁿh" "Ôⁿh" "Ǒⁿh" "Ōⁿh" "O̍ⁿh"])
 ("Oaih" ["Oaih" "Oáih" "Oàih" "Oaih" "Oâih" "Oǎih" "Oāih" "Oa̍ih"])

 ("Oh8" ["O̍h"])
 ("Ok8" ["O̍k"])
 ("Oah8" ["Oa̍h"])
 ("Oeh8" ["Oe̍h"])
 ("Oat8" ["Oa̍t"])

;;;;;; mng ;;;;;;
 ("m" ["m" "ḿ" "m̀" "m" "m̂" "m̌" "m̄" "m̍"])
 ("M" ["M" "Ḿ" "M̀" "M" "M̂" "M̌" "M̄" "M̍"])
 ("ng" ["ng" "ńg" "ǹg" "ng" "n̂g" "ňg" "n̄g" "n̍g"])
 ("Ng" ["Ng" "Ńg" "Ǹg" "Ng" "N̂g" "Ňg" "N̄g" "N̍g"])
 ("mh8" ["m̍h"])
 ("ngh8" ["n̍g"])
 ("Mh8" ["M̍h"])
 ("Ngh8" ["N̍g"])
)

(provide 'taigi-poj)
