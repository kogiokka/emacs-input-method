# Emacs 行列輸入法
行列輸入法是一套免費授權、自由開源的中文字形輸入法。<br />
本檔案使用 Quail 套件，將行列輸入法移植到 Emacs 上。<br />
涵蓋了一、二級簡碼，二碼以上的重碼候選字須鍵入空格後才能選擇。

## 安裝和使用
將`array30.el`檔案放置到`~/.emacs.d/private/`之中（或自訂路徑）

在設定檔（通常為`init.el`）添加下列代碼：
```
(package-initialize)
(add-to-list 'load-path "~/.emacs.d/private/")
(load "...其他輸入法檔案...")
(load "array30.el")`
```

輸入`M-x set-input-method`後選擇 `array30`<br />
透過`C-\`關閉或開啟輸入法

### Spacemacs
請至`~/.spacemacs`（指令`SPC f e d`）將代碼添加在`user-config`欄位：

```
(defun dotspacemacs/user-config ()
  "...說明文字..."
  (package-initialize)
  (add-to-list 'load-path "~/.emacs.d/private/")
  (load "...其他輸入法檔案...)
  (load "array30.el")
  )
```


## 檔案歷史
本 Emacs 輸入法的原始檔案取自 OpenVanilla 專案的 cin 檔，經過修改移植後以 GPLv3 發佈。

原始檔案說明節錄如下：

本 array30.cin 原始檔案是由霍東靈 Anthony Fok <foka@debian.org>
利用行列科技在 porting.exe 檔內提供之 array30.txt 及 arrayhw.txt
(05/31/1996) 轉換後，由葉光哲 (gontera) 根據行列科技廖明德先生
（即行列輸入法發明人）慷慨提供支援 unicode 3.1 中日韓漢字集共七萬
餘字的完整對照檔，製作而成。

本檔案跟行列科技所提供的原始檔案一樣公開授權，又或可依據 GNU
General Public License (GPL) version 2 或新版公開授權，歡迎各界分
布及使用。在此謹致謝行列科技之慷慨大方，讓大家可免費自由地使用行
列輸入法。


# Emacs Array30 Chinese input mehtod
Array30 is a free input method based on graphological aspect of Hanzi, mainly aiming for Traditional Chinese Characters.
